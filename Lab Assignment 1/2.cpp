//Documentation
/*
Assignment: Lab 1.2
Program: A menu driven program that would help the store to automate its sales process.
Author: Saloni Chudgar
Date: 10th January, 2017
*/
#include<iostream>
#include<cstdlib>
using namespace std;
class customer                          //Class for customer
{
	public:                             //Public Functions
		void Product(int, int);
		void GenerateBill();
		void CustomerDetails();

	private:                            //Private  Variables
		char CustomerName[30];
		unsigned long long int CustomerMobile;
		int Total=0;
};
void customer:: CustomerDetails()       //To store customer details
{
	cout<<"\nEnter the name of the customer: ";
    cin>>CustomerName;
	cout<<"\nEnter your Mobile Number: ";
	cin>>CustomerMobile;
}
void customer:: Product(int product, int quantity)  //To get total amount of balance
{
	Total=Total+(product*quantity);
}
void customer:: GenerateBill()          //To generate Bill
{
	cout<<"\nYour Bill\n";
	cout<<"\nName: "<<CustomerName;
	cout<<"\nMobile: "<<CustomerMobile;
	cout<<"\nTotal Purchase:"<<Total<<"Rs";
}
customer client[10];
int tot=0;
int main()
{
	int ProductPrice[7]={25000, 50000, 12500, 22500, 55500, 31500, 27500};
	int cnt;
	do
	{
		cout<<"\nEnter the number for the function you want to perform: \n1. To enter new customer \n0. Exit\n";
		cin>>cnt;
		if(cnt==1)
			{
				client[tot].CustomerDetails();
				int choice =0;
				int quantity;
				do
				{
					cout<<"\nEnter the number for the product you want to purchase: \n1. Washing Machine - Rs 25000\n2. Laptop - Rs 50000 \n3. Mobile - Rs 12500 \n4. Air Conditioner - Rs 22500 \n5. T.V. - Rs 55500 \n6. Refrigerator - Rs 31500 \n7. Microwave Oven - Rs 27500 \n0. Exit\n";
					cin>>choice;                //Menu Driven Program
					if(choice!=0)
					{
						cout<<"\nEnter the quantity: ";
						cin>>quantity;
					}
					client[tot].Product(ProductPrice[choice-1], quantity);
				}while(choice!=0);
				client[tot].GenerateBill();
				tot++;
			}
	}while(cnt!=0);
}