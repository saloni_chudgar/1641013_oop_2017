//Documentation
/*
Assignment: Lab 1.1
Program: A menu driven program that would help the bank in maintaining the details of the customer and the account transactions.
Author: Saloni Chudgar
Date: 10th January, 2017
*/
#include<iostream>
#include<cstdlib>
using namespace std;
int cnt=1;
class customer                      //Class for the customer
{
    public:                         //Public Functions
        void Deposit();
        void Withdraw();
        void Display();
        void ChangeDetails();
        void EnterDetails();


    private:                        //Private Variables
        char CustomerName[30];
        bool AccountType;
        int AccountNo;
        int AccountBalance;



};
void customer:: EnterDetails()      //Add a new customer
{
    cout<<"\nEnter the name of the client: ";
    cin>>CustomerName;
    cout<<"\nEnter the initial balance in their account: ";
    cin>> AccountBalance;
    cout<<"\nEnter the account type: 0. for Savings Account and 1. for Current Account: ";
    cin>>AccountType;
    AccountNo=cnt;
    cout<<"\nThe account number is: "<<AccountNo;
    cnt++;
}
void customer:: Display()            //View details of the customer
{
    cout<<"\nThe account name is: "<<CustomerName;
    cout<<"\nThe account number is: "<<AccountNo;
    cout<<"\nThe account type is: ";
    if(AccountType == 1)
    cout<<"Current Account";
    else
    cout<<"Savings Account";

    cout<<"\nThe account balance is: "<<AccountBalance;
}
void customer:: ChangeDetails()     //Edit details of the customer
{
    int choice=0;
    do
    {
        cout<<"\nEnter the number for the detail you want to change: \n1. Customer Name \n2. Account Type \n0. Exit";
        cin>>choice;
        switch(choice)
        {
            case 1:
                cout<<"\nEnter the new name: ";
                cin>> CustomerName;
            case 2:
                cout<<"\nEnter the account type: 0. for Savings Account and 1. for Current Account: ";
                cin>>AccountType;
        }
    }
    while(choice!=0);

}
void customer:: Deposit()       //Performs process of crediting money
{
    int money;
    cout<<"\nEnter the amount of money you want to deposit: ";
    cin>>money;
    AccountBalance=AccountBalance+money;
}
void customer:: Withdraw()      //Performs process of debiting money
{
    int money;
    cout<<"\nEnter the amount of money you want to withdraw: ";
    cin>>money;
    if(AccountBalance>money)
    AccountBalance=AccountBalance-money;
    else
    cout<<"***Insufficient Balance***\n\n\n";
}
customer client[50];           //A static instance of customer class
int main()
{
    int variable, AccNo;
    do
    {
        cout<<"\n1. Enter a new client\n2. Change Details of an existing client\n3. View Details of a client\n4. Withdraw money \n5. Deposit Money\n0. Exit\n";
        cin>>variable;
        switch(variable)      //Menu Driven Program
            {
                case 1:
                    client[cnt].EnterDetails();
                    break;
                case 2:
                    cout<<"Enter your account number: ";
                    cin>>AccNo;
                    client[AccNo].ChangeDetails();
					break;
                case 3:
                    cout<<"Enter your account number: ";
                    cin>>AccNo;
                    client[AccNo].Display();
					break;
                case 4:
                    cout<<"Enter your account number: ";
                    cin>>AccNo;
                    client[AccNo].Withdraw();
					break;
                case 5:
                    cout<<"Enter your account number: ";
                    cin>>AccNo;
                    client[AccNo].Deposit();
					break;
            }
    }while(variable!=0);

}
